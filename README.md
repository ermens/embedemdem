# Embedemdem #

_A little girl playing giant_

Embedemdem is an RSC (Ridiculously Small CMS)
It provides a bare base on wich a website can be build and tries to restrict to the absolute minimum.

Principles:

* Less is more / it can't break if it doesn't exist
* A backend is for content only
* Configuration doesn't belong in a backend
* Content administrators should not be busy with styling or layout
* Developers know how to find and edit php and configuration files
* Designers know how to find and edit template files and stylesheets
* Adding a content file should be enough to add a page
* Content should be easy editable directly in the content files
* Migrating a website should be no more difficult than downloading files and upload them elsewhere 

### What is this repository for? ###

A ridiculously small and bare CMS

### How do I get set up? ###

* Summary of set up
* Configuration
* Dependencies
* Database configuration
* How to run tests
* Deployment instructions

### Future ideas ###

* Taxonomies
* Blog functionality
* Shop functionalty(?)

### Todo ###

* Localization (setlocale())

### Answers ###
* **What is the relation between Embedemdem and embedding?**  
Answer: There is no relation. "Embedemdem" is the sound that my daughter made when she was a toddler and was playing a monster or a giant. Exactly what Embedemdem does; a little girl playing giant. 

### Who and where? ###

* Toine Ermens
* toine@ermens.net
