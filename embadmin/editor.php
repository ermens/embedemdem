<?php
require_once ('../embengine/config.php');
require_once(EMBD_ENGINE_PATH . 'functions.php');
if ( isset($_GET['page']) ) {
	$current_page = $_GET['page'];
	$page_title = mbdm_content_handler::generate_name( $current_page );
	$page_content = mbdm_page_content( $current_page);
	$has_page_param = true;
} else {
	$page_title = 'Editor';
	$has_page_param = false;
}
mbdm_admin_head();
?>
<header class="row">
	<div class="col-12">
		<a href="<?php echo mbdm_url(); ?>"><img src="<?php echo mbdm_admin_url('images'); ?>mbdm-logo.png"></a>
	</div><!-- end col -->
</header>
<nav class="row admin-nav">
<div class="col-12">
<?php mbdm_admin_menu(); ?>
</div><!-- end col -->
</nav>

<div class="row">
	<div class="col-12">
		<section>
			<h1><?php echo $page_title; ?></h1>
			<?php if ($has_page_param) { ?>
			
			<p>Below find the editable sections of page: <?php echo $page_title; ?></p>
			<form class="mbdm-admin-form">
				<p>
					<label for="title">Title</label>
					<input type="text" name="title" id="title" value="<?php echo $page_content['meta']['title'] ?>">
				</p>				
					<?php foreach ($page_content['body'] as $tag => $content) { ?>
					<div>
						<label for="<?php echo $tag; ?>"><?php echo $tag; ?></label>
						<textarea name="<?php echo $tag; ?>" id="<?php echo $tag; ?>"><?php echo $content; ?></textarea>
					</div>
					<?php } ?>				
				<p>
					<label for="description">Meta description</label>
					<input type="text" name="description" id="description" value="<?php echo $page_content['meta']['description']; ?>">
					<input type="hidden" name="template" value="<?php echo $page_content['meta']['template']; ?>">
				</p>

				<p><input type="submit" value="Save"></p> 
			</form>

			<p><?php // mbdm_file_handler::generate_page( $current_page ); ?>	
			
			<?php } else { ?>
				<p><?php echo 'You\'re in the content edit section. Choose a page to edit from the menu.'; ?></p>
			<?php } ?>
		</section>

	</div><!-- end col -->
</div><!-- end row -->

<?php mbdm_admin_foot(); ?>
