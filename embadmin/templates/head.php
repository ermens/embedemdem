<!DOCTYPE html>
<html>

<head>
    <title>Embedemdem</title>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" type="text/css" href="<?php echo mbdm_admin_url('styles'); ?>normalize.css">
    <link rel="stylesheet" type="text/css" href="<?php echo mbdm_admin_url('styles'); ?>grid.css">
    <link rel="stylesheet" type="text/css" href="<?php echo mbdm_admin_url('styles'); ?>style.css">
	<?php mbdm_admin_wysiwyg_style(); ?>
    <link rel="icon" type="image/png" href="<?php echo mbdm_admin_url('images/icons'); ?>embicon-32.png">
    <link rel="apple-touch-icon" href="<?php echo mbdm_admin_url('images/icons'); ?>embicon-48.png">
    <link rel="apple-touch-icon" sizes="152x152" href="<?php echo mbdm_admin_url('images/icons'); ?>embicon-152.png">
    <link rel="apple-touch-icon" sizes="167x167" href="<?php echo mbdm_admin_url('images/icons'); ?>embicon-167.png">
    <link rel="apple-touch-icon" sizes="180x180" href="<?php echo mbdm_admin_url('images/icons'); ?>embicon-180.png">
    <link rel="icon" sizes="192x192" href="<?php echo mbdm_admin_url('images/icons'); ?>embicon-192.png">
    <meta name="msapplication-square310x310logo" content="<?php echo mbdm_admin_url('images/icons'); ?>embicon-192.png">
</head>

<body>
<div class="container">
