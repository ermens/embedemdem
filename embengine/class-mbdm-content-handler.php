<?php
/*
	Content handler
	Deals with the content (xml) files
	origin: 0.1
	updated: 0.1
*/


final class mbdm_content_handler {

/*
generate_name()
in: filename
out: Nice name
*/

	public static function generate_name( $file=false ) {
		$replace_with_spaces = array (
			'-',
			'_'
		);
		$name = ucfirst( str_replace($replace_with_spaces, " ", basename($file, '.xml')));
		return $name;
	}

/*
get_page_content()
in: XML file in content directory
out:
	Array
	(
		[meta] => Array
		(   [template]
			[title]
			[description]
		)

		[body] => Array
		(
			[section-name 1]
			[section-name 2]
			[etc...]
		)
	)
 */

	public static function get_page_content( $page=false ) {
		/* Check if a page was given and if the content file exists */
		if ( !$page ) { // or !file_exists($content_path)
			return false;
		}
		$content_path = EMBD_PATH . 'content/' . $page;
		$page_content = simplexml_load_file( $content_path ) or die("Error: Cannot load object");

		/* First create an array for META
		   Associative - 1 dimension (Meta[key] > value)
			Meta
				-[template] > template file
				-[title] > page title
				-[description] > page description
		*/
		$page_meta = json_decode(json_encode($page_content->meta), true);


		/* Now create an array for the BODY
		   Associative 1 dimension (Body[section name] > content)
			Body
				- [name1] > content 1
				- [name2] > content 2
				- [name3] > content 3
		*/
		$page_body = array();

		foreach ($page_content->body->section as $section) {
			$section_name = (string) $section['name'];
			$section_content = trim((string) $section);
			$page_body[$section_name] = $section_content;
		}
		/*
			Finally add both (page_meta and page_body) arrays in one main array (page_content)
		*/
		$page_content = array(
			'meta'=> $page_meta,
			'body'=> $page_body
		);
		return $page_content;
	}


/*
Read the files in the content directory (the pages)
in: nothing
out:
	Array
	(
		[0] => Array
			(
				[file] => page_one.xml
				[permalink] => page_one
				[name] => Page one
			)
			(
				[file] => page_two.xml
				[permalink] => page_two
				[name] => Page two
			)
*/
	public static function get_content_files() {
		$content_dir = EMBD_CONTENT_PATH;
		$dir_content = array();
		$ignore = array (
			'.',
			'..',
			'index.php'
		);

		// Open content directory, and read its content
		if (is_dir($content_dir)){
		  if ($dh = opendir($content_dir)){
			while (($file = readdir($dh)) !== false){
				if ( !in_array($file, $ignore) ) {
					$page_data = array(
						'file'		=>	$file,
						'permalink'	=>	basename($file, '.xml'),
						'name'		=>	self::generate_name($file)
						// 'name'		=>	ucfirst( str_replace($replace_with_spaces, " ", basename($file, '.xml')))
					);
					array_push( $dir_content, $page_data ); // fill array
				}

			}
			closedir($dh);
		  }
		 sort($dir_content);
		}
		return $dir_content;
	}


/*
get_json_page_content()
Gets the page content form a json file
in: Json file in content directory
out:
	Array
	(
		[meta] => Array
		(   [template]
			[title]
			[description]
		)

		[body] => Array
		(
            [0] => Array
                (
                    [name]
                    [content]
                )

            [1] => Array
                (
                    [name]
                    [content]
                )
			[3] => Array
                (
					[name]
					etc...
				)
		)
	)
 */

	public static function get_json_page_content( $page=false ) {
		/* Check if a page was given and if the content file exists */
		if ( !$page ) { // or !file_exists($content_path)
			return false;
		}
		$content_path = EMBD_PATH . 'content/' . $page;
		$data = file_get_contents($content_path);
		$pagedata = json_decode($data, true);
		
		print_r($pagedata->meta);
		print_r($pagedata->body);


}
