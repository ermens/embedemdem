<?php
/*
	Authenticator
	Handles user authentication
	origin: 0.1
	updated: 0.1
*/

final class mbdm_auth {

	/*
	generate_id()
	Generate a unique (user) ID
	in: nothing
	out: Unique ID
	*/
	public static function generate_id() {	  
		# create random key
		$rootstr = 'abcdefghijklmnopqrstuvwxyz1234567890';
		$randomkey = substr(str_shuffle($rootstr),0,8);
		# Create unique id with random key prefix
		return uniqid($randomkey);
	}
  
	/*
	hash_passwd()
	Create a salted password hash
	in: password
	out: passwordhash
	     False if no password given
	*/
	public static function hash_passwd($passwd = NULL) {
		if (empty($passwd)) {
			return false;
		}
		return password_hash($passwd, PASSWORD_DEFAULT);
	}

	/*
	check_passwd()
	check a password
	in: password
	out: True if password OK
	     False if password NOT OK or no password given
	*/
	public static function check_passwd($passwd = NULL) {
		if (empty($passwd)) {
			return false;
		}
		// lookup the passwordhash from user and store it in $passwordhash
		if (password_verify($password, $passwordhash) {
			return true;
		} else {
			return false;
		}
		
	}


}
