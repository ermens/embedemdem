<?php

/* Includes */
function mbdm_includes() {
	require_once(EMBD_ENGINE_PATH . 'class-mbdm-content-handler.php');
	require_once(EMBD_ENGINE_PATH . 'class-mbdm-file-handler.php');
	require_once(EMBD_ENGINE_PATH . 'class-mbdm-hook-register.php');
}


/* ---------------
mbdm_host_url()

WARNING: Don't use this function in templates or extensions.
This is an internal function in order to define the EMBD_URL constant
The return value changes depending om the location were it was called from.
Use mbdm_url() instead!

in:
	Nothing
out:
	the base URL for embedemdem, preceded by the protocol
*/
function mbdm_host_url() {
	if ( empty($_SERVER['HTTPS']) ) {
		$protocol = 'http';		
	} else {
		$protocol = 'https';
	}
	$mbdm_host_url = $protocol. '://' . $_SERVER['HTTP_HOST'] . '/';

	return $mbdm_host_url;
}

/* ---------------
mbdm_url()
Returns the base URL of the website (e.g. https://www.domain.com/)
in:
	Path to be added to the url (optional)
out:
	the base URL for embedemdem with a trailing slash, preceeded by the protocol
	or false if constant EMBD_URL was not defined yet
*/
function mbdm_url( $path=NULL ) {
	if( defined ('EMBD_URL') ) {
		$mbdm_url = EMBD_URL . '/';
			
		if ( !empty($path) ) {
			$mbdm_url .= $path . '/';
		}
		return $mbdm_url;
	} else {
		return false;
	}
}


/* ---------------
mbdm_admin_url()
Returns the admin URL of the website (e.g. https://www.domain.com/)
in:
	Path to be added to the admin url (optional)
out:
	the base URL for the embedemdem admin with a trailing slash, preceeded by the protocol
*/
function mbdm_admin_url( $path=NULL ) {
	$mbdm_admin_url = mbdm_url('embadmin');
	if ( !empty($path) ) {
		$mbdm_admin_url .= $path . '/';
	}
	return $mbdm_admin_url;
}

/* ---------------
mbdm_admin_menu()
Parses the main menu in the admin page
in:
	nothing
out:
	nothing
*/
function mbdm_admin_menu() {
	$mbdm_admin_menu = mbdm_content_handler::get_content_files();
	echo '<ul id="embadmin-menu">' . "\n";
	echo "\t" . '<li><a href="' . mbdm_admin_url() . 'index.php/">Dashboard</a></li>' . "\n";
	foreach ($mbdm_admin_menu as $item) {
		echo "\t" . '<li><a href="' . mbdm_admin_url() . 'editor.php/?page=' . $item['permalink'] . '">' . $item['name'] . '</a></li>' . "\n";
	}
	echo '</ul>' . "\n";
}

function mbdm_page_content( $page=NULL ) {
/* ---------------
Returns the content of a given page
in:
	Filename of a page (xml file in content directory)
out:
	Array
	(
		[meta] => Array
		(   [template]
			[title]
			[description]
		)

		[body] => Array
		(
			[section-name 1]
			[section-name 2]
			[etc...]
		)

*/
	if ( isset($page) ) {
		return mbdm_content_handler::get_page_content($page . '.xml');
		
	} else {
		return false;
	}
}

function mbdm_head( $head=NULL ) {
/* ---------------
Returns the head section for a page template
in:
	Filename of a (sub)template for the head section
out:
	Head section for HTML
	false if failed
*/
$head = '<html>';
$head .= "\n";
return $head;

}

function mbdm_foot( $foot=NULL ) {
/* ---------------
Returns the foot section for a page template
in:
	Filename of a (sub)template for the head section
out:
	Foot section for HTML
	false if failed
*/
$foot = "\n";
$foot .= '</html>';
return $foot;

}

function mbdm_section( $section=NULL ) {
/* ---------------
Echoes a section from the content (xml) file for a page template
in:
	Filename of a page (xml file in content directory)
out:
	true if succesful
	false if failed
*/
	if ( empty($section) ) {
		return false;
	}
	echo 'Content of ' . $section;
	// of course we need to find the section and print it here.
	
}

function mbdm_render_html( $page=NULL ) {
/* ---------------
Renders a final HTML file from a content (xml) file.
in:
	Filename of a page (xml file in content directory)
out:
	true if succesful
	false if failed
*/
	if ( empty($page) ) {
		return false;
	}
	$page_content = mbdm_content_handler::get_page_content($page . '.xml');
	

// dan de php template ophalen
// Dan met copy de html maken

}

/* Generate password hash 
	in: password
	out: password hash or false (if no password given)
*/
function mbdm_generate_passwd_hash( $password = false ) {
	if ( !$password ) {
		return false;
	}
	return password_hash($password, PASSWORD_BCRYPT);
}

/* Check password
	in: Username and given password
	out: true (match) or false (no match)
 */
function mbdm_check_passwd ( $username = false, $password = false ) {
	if ( !$username or !$password) {
		return false;
	}
	// lookup username in users/id.txt and store corresponding hash in $hash
		
	if (password_verify($password, $hash)) {
		return true;
	} else {
		return false;;
	}
		
}

/* ---------------
mbdm_add_hook_action()
Add a function to a hook
in:
	$hook		>	string: Hook name
	$function	>	string: Function name
	$params		>	array: Function parameters
out:
	true if registered correctly
	false if not registered (hook or function are not given or params are given but not as array)
*/
function mbdm_add_hook_action( $hook = NULL, $function = NULL, $params = NULL ) {
	return mbdm_hook_reg::add($hook, $function, $params);
}

/* ---------------
mbdm_remove_hook_action()
Removes a function from a hook
in:
	$hook		>	string: Hook name
	$function	>	string: Function name
out:
	true if unregistered correctly
	false if not unregistered (e.g. hook or function does not exist)
*/
function mbdm_remove_hook_action( $hook = NULL, $function = NULL ) {
	return mbdm_hook_reg::remove($hook, $function);
}

/* ---------------
mbdm_run_hook()
Fires a hook
in:
	$hook		>	string: Hook name
out:
			true if the hook runned
			false if the hook did not run (e.g. hook does not exist)
*/
function mbdm_run_hook( $hook = NULL ) {
	return mbdm_hook_reg::run($hook);
}

/* ---------------
mbdm_dump_hook()
Removes a function from a hook
in:
	nothing
out:
	entire hook register array
*/
function mbdm_dump_hook() {
	echo 'Hallo';
	return mbdm_hook_reg::dump();
}

/* ---------------
mbdm_admin_head()
Parses the HTML head for the admin pages
in:
	nothing
out:
	nothing
*/
function mbdm_admin_head() {
	require_once (EMBD_ADMIN_PATH . 'templates/head.php');	
}

/* ---------------
mbdm_admin_foot()
Parses the HTML footer for the admin pages
in:
	nothing
out:
	nothing
*/
function mbdm_admin_foot() {
	require_once (EMBD_ADMIN_PATH . 'templates/foot.php');	
}
/* ---------------
mbdm_admin_wysiwyg_style()
Parses the path to the stylesheet for the wysiwyg editor
in:
	nothing
out:
	nothing
*/
function mbdm_admin_wysiwyg_style() {
	echo '<link rel="stylesheet" type="text/css" href="' . mbdm_admin_url('trumbowyg/ui') . 'trumbowyg.min.css">' . "\n";
	echo '<link rel="stylesheet" type="text/css" href="' . mbdm_admin_url('trumbowyg/ui') . 'trumbowyg.table.min.css">' . "\n";
}

/* ---------------
mbdm_admin_wysiwyg_script()
Parses the path to the stylesheet for the wysiwyg editor
in:
	nothing
out:
	nothing
*/
function mbdm_admin_wysiwyg_script() {
	echo '<script src="' . mbdm_admin_url('trumbowyg') . 'trumbowyg.min.js"></script>' . "\n";
	echo '<script src="' . mbdm_admin_url('trumbowyg') . 'trumbowyg.table.min.js"></script>' . "\n";
}

/* ---------------
mbdm_admin_jquery()
Parses the path to jquery
in:
	nothing
out:
	nothing
*/
function mbdm_admin_jquery() {
	echo mbdm_admin_url('javascript') . 'jquery.min.js';
}