<?php
/*
	File handler
	Manages all file handling
	origin: 0.1
	updated: 0.1
*/


final class mbdm_file_handler {

	
	public static function generate_page( $page=NULL ) {
	/*
	Generate a static HTML page
	in: 	XML file in content directory
	out:	True if succesful
			False if not succesful

	*/		
		if ( !isset($page) ) {
			return false;
		}
		// Get content from XML file
		$page_content = mbdm_page_content($page);

		if ( $page_content === false ) {
			return false;
		}
		$page_template = $page_content['meta']['template'];
		$page_title = $page_content['meta']['title'];
		$page_description = $page_content['meta']['description'];
		
		// Get template
		$html_content = file_get_contents( EMBD_TEMPLATE_PATH . $page_template );
		// replace content in $template_content with body section and store it
		foreach ($page_content['body'] as $tag => $content) {
			$template_tag = '[[' . $tag . ']]' ;
			$section_element = "\n<" . SECTION_TAG . ' class="' . CLASS_PREFIX . $tag . "\">\n" . trim($content) . "\n</" . SECTION_TAG . '><!--' . CLASS_PREFIX . $tag . " -->\n";
			$html_content = str_replace ($template_tag, $section_element, $html_content);
		}
		// Build entire page
		if ( $page == 'frontpage' ) {
			$page = 'index';
		}
		$html_page = mbdm_head();
		$html_page .= $html_content;
		$html_page .= mbdm_foot();
		$html_file = EMBD_PATH . $page . '.html';
		file_put_contents($html_file, $html_page);
		chmod($html_file, 0664);

		echo $html_page;
		// $search_tag = strtolower 
		// trim spaces?
		
		
	}

}