<?php

/* ===== Settings ===== */

/* enclosing HTML tag for content sections */
define ('SECTION_TAG', 'div');

/* Prefix for CSS class enclosing HTML tag*/
define ('CLASS_PREFIX', 'embd-');

/* ===== Debugging ===== */

/* Enable or disable debug modus */
define('EMBD_DEBUG', false);

/* Enable or disable logging */
define('EMBD_LOG', false);

/* ===== Paths ===== */

/* Absolute path to Embedemdem */
define ('EMBD_PATH', dirname(__FILE__, 2) . '/');

/* Absolute path to Embedemdem engine */
define ('EMBD_ENGINE_PATH', dirname(__FILE__) . '/');

/* Absolute path to Embedemdem admin */
define ('EMBD_ADMIN_PATH', EMBD_PATH . 'embadmin/');

/* Absolute path to Embedemdem templates */
define ('EMBD_TEMPLATE_PATH', EMBD_PATH . 'templates/');

/* Absolute path to Embedemdem content */
define ('EMBD_CONTENT_PATH', EMBD_PATH . 'content/');

/* ===== Functions ===== */
require_once(EMBD_ENGINE_PATH . 'functions.php');


/* ===== Includes ===== */

mbdm_includes();

/* ===== Init ===== */

define ('EMBD_URL', mbdm_host_url() . 'embedemdem');
define ('EMBD_ADMIN_URL', mbdm_host_url() . 'embedemdem/admin');
