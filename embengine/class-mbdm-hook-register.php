<?php
/*
	Hook register	
	keeps track of all registered hooks
	origin: 0.1
	updated: 0.1
*/

final class mbdm_hook_reg {
	
	/* ---------------
	Array
	(
    [hookname_one] => Array
        (
            [0] => Array
                (
                    [0] => function_name_one
                    [1] => Array
                        (
                            [0] => param_one
                            [1] => param_two
                        )
                )

			[1] => Array
                (
                    [0] => function_name_two
                    [1] => Array
                        (
                            [0] => param_one
                            [1] => param_two
                        )
                )	
        )

	[hookname_two] => Array
        (
            [0] => Array
                (
                    [0] => function_name_one
                    [1] => Array
                        (
                            [0] => param_one
                        )
                )

			[1] => Array
                (
                    [0] => function_name_two
                )	
        )
	)
	etc.
	*/
	
	
	private static $hook_reg = array();
	
	/* ---------------
	read()
	Returns all function for a given hook
	
	in:
		$hook >	string: Hook name
		
	out: Array with registered functions for given hook
	
		[Hookname] => Array
			(
				[0] => Array
					(
						[0] => FirstFunction
					)

				[1] => Array
					(
						[0] => SecondFunction
						[1] => Array                        
							(
								[0] => FirstParam
								[1] => SecondParam  
								[2] => ThirdParam							
							)

					)
			)
		
		False if hook does not exist, no hook was given or no functions are found
		(Note that a hook does not exist if no functions were attached to it or all functions were
		removed from it.)
	*/
		
	private static function read( $hook = NULL ) {
		if ( empty($hook) or !array_key_exists($hook, self::$hook_reg) ) {
				return false; // no hook was given or hook does not exist
			}				
			return self::$hook_reg[$hook];
	}	
	
	/* ---------------
	add()
	Add a function to a hook
	in:
		$hook		>	string: Hook name
		$function	>	string: Function name
		$params		>	array: Function parameters
	out:
		true if registered correctly
		false if not registered (hook or function are not given, function does not exist or params are given but not as array)
	*/
		
	public static function add( $hook = NULL, $function = NULL, $params = NULL ) {
		if ( empty($hook) or empty($function) ) {
			return false; //hook or function misses
		} elseif ( !empty($params) and !is_array($params) ) {
			return false; //param given but not as array
		} elseif( !is_callable($function) ) {
			return false; // No such function exists
		}
		
		$new_registration_function = array($function); // Create temp array for functio
		if ( !empty($params) ) {
			array_push($new_registration_function, $params); // add params if given
		}
				
		if (array_key_exists($hook, self::$hook_reg) ) { // Register key already exists, so add it
			array_push(self::$hook_reg[$hook], $new_registration_function);
		} else { // Register key does not exists, so create it
			self::$hook_reg[$hook] = array($new_registration_function);
		}
		asort (self::$hook_reg);
	
		return true;		
	}
		
	/* ---------------
	remove()
	Removes a function from a hook
	in:
		$hook		>	string: Hook name
		$function	>	string: Function name
	out:
		true if unregistered correctly
		false if not unregistered (e.g. hook or function does not exist)
	*/
		
	public static function remove( $hook = NULL, $function = NULL ) {
		if ( empty($hook) or empty($function) or !array_key_exists($hook, self::$hook_reg) ) {
			return false; //hook or function misses
		} elseif ( !array_key_exists($hook, self::$hook_reg) ) {
			return false; //hook does not exist
		} elseif ( array_search($function, array_column(self::$hook_reg[$hook], 0)) === false ) {
			return false; //No such function attached to this hook
		}
		unset(self::$hook_reg[$hook][array_search($function, array_column(self::$hook_reg[$hook], 0))]); // Remove array key from hook
		if ( sizeof(self::$hook_reg[$hook]) < 1 ) {
			unset(self::$hook_reg[$hook]); // If size of hook array is 0 remove entire hook
		}
	}
	
	/* ---------------
	run()
	Run a given hook
	in:
		$hook > string: Hook name
	out:
		true if the hook runned
		false if the hook did not run (e.g. hook does not exist)
	*/

		public static function run( $hook = NULL ) {
			if ( empty($hook) or !array_key_exists($hook, self::$hook_reg) ) {
				return false; //hook misses or does not exist
			} else {
				$callbacks = self::read($hook); // Get all callback functions for this hook
				foreach ( $callbacks as  $callback ) {
					if( is_callable($callback[0]) ) { // Given callback is available
						if ( sizeof($callback) == 1 ) {
							$callback[1] = array(NULL); // No parameters for this function, but we need to pass an array to call_user_func_array
						}
						call_user_func_array($callback[0], $callback[1]); // Run the callback
					}
				}

			}
		}
	/* ---------------
	dump()
	Dump the entire hook register array
	in:
		nothing
	out:
		entire hook register array
	*/		
	
	public static function  dump() {
		return self::$hook_reg;
	}
}

